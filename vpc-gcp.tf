provider "google" {
  version = "3.25.0"
  credentials = "./creds/serviceaccount.json"
  project = "terraform-poc-279707"
  region  = "asia-south1"
  zone    = "asia-south1-a"
}
// Create Dev-VPC
resource "google_compute_network" "vpcdev" {
 name                    = "dev-vpc"
 auto_create_subnetworks = "false"
}
// Subnets for Dev
// Create Public-Subnet
resource "google_compute_subnetwork" "subnet-dev-a" {
 name          = "pub-subnet-dev-a"
 ip_cidr_range = "10.6.1.0/24"
 network       = "dev-vpc"
 depends_on    = ["google_compute_network.vpcdev"]
 region        = "us-central1"
}
// Create public-subnet
resource "google_compute_subnetwork" "subnet-dev-b" {
 name          = "pub-subnet-dev-b"
 ip_cidr_range = "10.6.2.0/24"
 network       = "dev-vpc"
 depends_on    = ["google_compute_network.vpcdev"]
 region      = "us-central1"
}
// Create Private-Subnet
resource "google_compute_subnetwork" "subnet-dev-c" {
 name          = "pvt-subnet-dev-c"
 ip_cidr_range = "10.6.3.0/24"
 network       = "dev-vpc"
 depends_on    = ["google_compute_network.vpcdev"]
 region      = "us-central1"
}
// Create Private-Subnet
resource "google_compute_subnetwork" "subnet-dev-d" {
 name          = "pvt-subnet-dev-d"
 ip_cidr_range = "10.6.4.0/24"
 network       = "dev-vpc"
 depends_on    = ["google_compute_network.vpcdev"]
 region      = "us-central1"
}
